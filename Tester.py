import string, sys, itertools
from EarleyParser import * 
from ChartItem import *

## convert tree
## calculate precision and recall for it


class Node:

	def __init__(self,name,children):
		self.lhs = name
		self.children = []

	# http://cbio.ufs.ac.za/live_docs/nbn_tut/trees.html
	def __repr__(self, level=0):
		ret = "\t"*level+repr(self.lhs)+"\n"
		for child in self.children:
			ret += child.__repr__(level+1)
		return ret

	def getAllChildrenAsString(self):
		ret = self.lhs + " "
		for child in self.children:
			ret += child.getAllChildrenAsString()
		return ret

class Tester:

	def __init__(self,pCFG,testSentences,testParseTrees):
		## setup variables
		self.testSentences = testSentences
		self.testParseTrees = testParseTrees
		self.punctuation = set(string.punctuation)
		self.punctuation.remove('&')
		self.punctuation.remove('*')
		self.pCFG = pCFG
		## test metrics
		self.recall = 0.0
		self.precision = 0.0
		self.f1 = 0.0
		## create parser grammar
		self.parserGrammar = Grammar()
		self.parserGrammar.addRules(self.pCFG.cfg)
		## run tests
		self.runTests()

	##
	# Takes in a trainingSet, Parser and a PCFG and tests the performance 
	# calculates precision, recall and f1 rate
	##
	def runTests(self):		
		## iterate through each file in the test set of untagged sentences
		for i in range(len(self.testSentences)):
			unparsedFile = self.testSentences[i]
			parsedFile = self.testParseTrees[i]
			if (len(parsedFile) != len(unparsedFile)):
				sys.exit('Parsed File and Unparsed File Do Not Match. Check creation of tester in main.py')
			## iterate through each sentence (which is a list of tokens)
			for j in range(len(unparsedFile)):
				sentence = unparsedFile[j]
				humanParseTree = self.convertNLTKTree(parsedFile[j])
				## stitch tokens together while removing tokens that are just punctuation and make tokens lowercase
				sentence = ' '.join(ch.lower() for ch in sentence if ch not in self.punctuation and not self.onlyPunctuation(ch))
				## generate parse trees for that sentence
				earleyParser = EarleyParser(sentence, self.parserGrammar.rules, self.parserGrammar.posRules)
				allTrees = earleyParser.earleyParse()
				if (allTrees == None):
					print('No Parse Found')
				else:
					## get the most likely parse tree from the parser
					systemParseTree = allTrees
					# systemParseTree = self.pCFG.getMostLikelyParse(list(allTrees))
					## run comparison
					f1, recall, precision = self.compareSystemAndHumanParses(systemParseTree,humanParseTree)
					print('Recall is:',recall,'%')
					print('Precision is:',precision,'%')
					print('F1 Metric is:',f1)

	##
	# Takes in a system parse tree and a parse tree from the treebank and compares them
	# returns a tuple containing recall and precision values
	##
	def compareSystemAndHumanParses(self,systemParse,humanParse):
		## get all the symbols in both trees
		humanSymbols = humanParse.getAllChildrenAsString().split()
		systemSymbols = systemParse.getAllChildrenAsString().split()
		print(humanSymbols)
		## set variables for precision and recall
		numCorrect = 0.0
		numTotalSystem = len(systemSymbols)
		numTotalHuman = len(humanSymbols)
		for symbol in humanSymbols:
			if symbol in systemSymbols:
				numCorrect += 1
				systemSymbols.remove(symbol)
		## return recall and precision
		print(numCorrect,numTotalSystem,numTotalHuman)
		recall = (numCorrect/numTotalHuman)
		precision = (numCorrect/numTotalSystem)
		return (2*recall*precision)/(recall+precision), recall, precision

	def convertNLTKTree(self,node,leaves=None):
		if leaves == None:
			leaves = node.leaves()
		if not self.onlyPunctuation(node.label()):
			convertedNode = Node(node.label(),[])
			rhs = []
			for child in node:
				if child in leaves and not self.onlyPunctuation(child):
					addedNode = Node(child,[])
					convertedNode.children.append(addedNode)
				elif not self.onlyPunctuation(child.label()):
					convertedNode.children.append(self.convertNLTKTree(child,leaves))
		return convertedNode

	##
	# Helper method, takes in a string and returns True if
	# it contains only punctuation
	##
	def onlyPunctuation(self,inputStr):
		return not any(char.isalnum() for char in inputStr)

	def getParentAnnotatedLine(self,line):
		for word in line:
			print(word)
