import string, math

class ProbCFG:

	def __init__(self):
		## training variables
		self.name = 'Probabilistic Context Free Grammar'
		self.punctuation = set(string.punctuation)
		self.punctuation.remove('&')
		self.punctuation.remove('*')		
		self.cfg = {
			"<UNK>": { "rules":[], "count":0 }
		}
		self.totalRules = 0

	##
	# Takes a parsed sentence of the NLTK Treebank Corpus and adds
	# the rules and their counts to a dictionary
	##
	def addRules(self,tree):
		leaves = tree.leaves()
		## iterate through each line
		for subtree in tree.subtrees():
			## set the LHS symbol
			lhs = subtree.label()
			if (not self.onlyPunctuation(lhs)):
				rhs = []
				for token in subtree:
					if token in leaves:
						if token not in self.punctuation and not self.onlyPunctuation(token):
							rhs.append(token.lower())
					elif token.label() not in self.punctuation and not self.onlyPunctuation(token.label()):
						rhs.append(token.label())
				## if something goes to an emp
				if rhs != []:
					if self.isMixedRule(rhs):
						self.addMixedRule(lhs,rhs)
					else:
						self.addRuleToCFG(lhs,rhs)

	##
	# Helper method, takes in a string and returns True if
	# it contains only punctuation
	##
	def onlyPunctuation(self,inputStr):
		return not any(char.isalnum() for char in inputStr)
	##
	# rhs is a list of symbols
	# objList is a list of rhs objects
	# returns the index of rhs in objList if rhs is in objList
	# returns None if rhs not in objList
	##
	def rhsInList(self,rhs,objList):
		for i in range(len(objList)):
			if (objList[i]['rhs'] == rhs):
				return i
		return None

	##
	# takes in a pCFG (dictionary) and uses counts to create
	# probabilities
	# returns pCFG with probabilities
	##
	def setPCFGProbs(self):
		print('Setting Probabilities for ',self.name)
		for lhs in self.cfg:
			for i in range(len(self.cfg[lhs]['rules'])):
				self.cfg[lhs]['rules'][i]['prob'] = math.log(self.cfg[lhs]['rules'][i]['count']/self.totalRules)

	##
	# takes in a lhs (string), and rhs (list) and looks through the grammar
	# to find the prob of lhs -> rhs and returns it. Returns None if rule not found
	##
	def getRuleProb(self,lhs,rhs):
		for rule in self.cfg[lhs]['rules']:
			if (rule['rhs'] == rhs):
				return rule['prob']
		return None

	## 
	# takes in the rhs of a rule to see if it contains a mix of POS and Non-POS
	# returns true if it contains POS and Non-POS
	##
	def isMixedRule(self,rhs):
		pos = False
		nonPos = False
		for symbol in rhs:
			if (symbol.islower()):
				nonPos = True
			elif (symbol.isupper()):
				pos = True
		if pos and nonPos:
			return True
		else:
			return False

	##
	# takes in a rule with that has both pos and nonpos on the rhs and converts
	# to a rule with just pos and a dummy rule
	# A -> B and C
	# turns into A -> B AND C + AND -> and
	# adds the rules to the grammar
	##
	def addMixedRule(self,lhs,rhs):
		modifiedRhs = []
		for symbol in rhs:
			# non pos: add dummy rule
			if symbol.islower():
				# add dummy rule to grammar
				self.addRuleToCFG(symbol.upper(),symbol)
				modifiedRhs.append(symbol.upper())
			else:
				modifiedRhs.append(symbol)
		addRuleToCFG(lhs,modifiedRhs)

	##
	# adds rule to pCFG and increments total rules count
	##
	def addRuleToCFG(self,lhs,rhs):
		## if we have already seen the LHS symbol before
		if lhs in self.cfg:
			## if we have also seen the RHS symbols before i.e. seen the rule before
			# if rhs in self.cfg[lhs]['rules']:
			listIndex = self.rhsInList(rhs,self.cfg[lhs]['rules'])
			if listIndex != None:
				self.cfg[lhs]['rules'][listIndex]['count'] += 1
			## if we have seen the LHS symbol but not this rule
			## i.e. first time seeing terminal
			else:
				self.cfg[lhs]['rules'].append({ "rhs": rhs, "count": 1 })
				self.addUNKCount(rhs)
			self.cfg[lhs]['count'] += 1
		## if we haven't seen the LHS symbol before
		## or the RHS before
		else:
			self.cfg[lhs] = {
				"rules":[{ "rhs": rhs, "count": 1 }],
				"count":1
			}
			self.addUNKCount(rhs)
		self.totalRules += 1


	##
	# Checks to see if RHS is just one terminal, if it is, adds count to dictionar
	##
	def addUNKCount(self,rhs):
		## one terminal
		if len(rhs) == 1:
			## terminals are all lower case
			if rhs[0].islower():
				## if it is already in dictionary
				listIndex = self.rhsInList(rhs,self.cfg['<UNK>']['rules'])
				if listIndex != None:
					self.cfg['<UNK>']['rules'][listIndex]['count'] += 1
				else:
					self.cfg['<UNK>']['rules'].append({
							"rhs":rhs,
							"count":1
						})
				self.cfg['<UNK>']['count'] += 1

	##
	# takes a list of parse trees and calculates the probability of each tree
	# returns the most likely tree and its log probability
	##
	def getMostLikelyParse(self,trees):
		maxProb = -float('inf')
		mostLikelyParse = None
		for tree in trees:
			treeProb = self.getTreeProbRec(tree,None)
			if treeProb > maxProb:
				maxProb = treeProb
				mostLikelyParse = tree
		return (mostLikelyParse,maxProb)

	##
	# helper method that recursively calculates probability of the parse tree
	##
	def getTreeProbRec(self,node,parent):
		## if node is terminal
		if (node.children == None or len(node.children) == 0):
			return 0
		## if not non-terminal
		else:
			rhs = [child.lhs for child in node.children]
			childProb = 0
			for child in node.children:
				childProb += self.getTreeProbRec(child,node)
			return math.log(self.getRuleProb(node.lhs,rhs)) + childProb