

import sys
import math
from queue import *
from collections import defaultdict
import pprint
import copy
from multiprocessing import Process, Lock
from threading import Thread, Semaphore
import time

class Grammar:
    ##
    # Storing rules in a dictionary
    # { 'S' : [ ['NP','VP'] ]}
    ##



    ##
    # is of the form
    # {
    #    "S":{ "rules":[ { "rhs":[ "NP", "VP" ], "count":2 },
    #                    { "rhs":[ "NP",  "S" ], "count":8 }
    #                  ], "count":10 }
    # }
    ##
    def __init__(self):
        self.rules = defaultdict(list)
        self.posRules = defaultdict(dict)

    def isTerminal(self, rhs):
        return (len(rhs) ==  1 and (rhs[0].islower() or rhs[0] == '&'))

    def addRules(self,rulesToadd):
        for lhs in rulesToadd.keys():
            rhSides = rulesToadd[lhs]
            for rhs in rhSides["rules"]:
                rhSide = rhs["rhs"]
                prob = rhs["prob"]
                if self.isTerminal(rhSide):
                    terminal = rhSide[0].strip()
                    self.posRules[terminal][lhs] = prob
                    # if lhs in self.posRules:
                    #     self.posRules[terminal][lhs] = prob
                    #     #self.posRules[terminal].append((lhs, prob))
                    # else:
                    #     self.posRules[terminal] = [(lhs,prob)]
                else:

                    if lhs in self.rules:
                        self.rules[lhs].append((rhSide, prob))
                    else:
                        self.rules[lhs] = [(rhSide, prob)]
    def addRules2(self, fakeParam):
        self.posRules["pierre"] =["NNP"]
        self.posRules["vinken"] = ["NNP"]
        self.posRules["as"] =["IN"]
        self.rules["S"].append(["NP-SBJ", "NP-PRD"])
        self.rules["NP-SBJ"].append(["NNP", "NNP"])
        self.rules["NP-PRD"].append(["NP", "PP"])
        self.rules["NP"].append(["NNP", "NNP"])
        self.rules["PP"].append(["IN", "NP"])

        # trees = [ Node('S',[ Node('NP-SBJ',[ Node('NNP', [Node ('pierre',None)]), Node('NNP', [Node ('vinken',None)]) ]),
    # 		  Node('NP-PRD', [
    # 							Node ('NP', [ Node('NNP', [Node ('pierre',None)]), Node('NNP', [Node ('vinken',None)]) ]),
    # 							Node ('PP', [
    # 											Node('IN',[Node('as',None)]),
    # 											Node ('NP', [ Node('NNP', [Node ('pierre',None)]), Node('NNP', [Node ('vinken',None)]) ])
    # 										])
    # 						  ])
    # 					]),

##
# Earley Algorithm Parser
# returns a 'chart'
##

class ChartItem:
    def __init__(self, lhs, rhs, indices, prob):
        self.lhs = lhs
        self.rhs = rhs
        self.indices = indices
        # self.subtrees = [[] for i in range(len(self.rhs)-1)] if type(rhs) is list else None
        #self.children = []
        self.children = [None for i in range(len(self.rhs)-1)] if type(rhs) is list else []
        self.stringVersion = self.lhs + "| [" + ", ".join(self.rhs)  + " ] | (" + ", ".join(map(str, self.indices)) + ")"
        self.prob = prob


    def addChild(self,child, rhsIndex):
        if self.children[rhsIndex] == None:
            self.children[rhsIndex] = child
            self.prob = self.prob + child.prob
        elif self.children[rhsIndex].prob < child.prob:
            self.prob = self.prob - self.children[rhsIndex].prob + child.prob
            self.children[rhsIndex] = child


        #self.subtrees[rhsIndex].append(child)


    def swapDot(self, indexRightOfDot):
        self.rhs[indexRightOfDot], self.rhs[indexRightOfDot-1] = self.rhs[indexRightOfDot -1], self.rhs[indexRightOfDot]
    def incrementSecondNumber(self):
        self.indices[1] += 1
    def replaceSecondNumber(self, otherChartItem):
        self.indices[1] = otherChartItem.indices[1]


    def __eq__(self, other):
        if other == None:
            return False
        else:
            return self.lhs == other.lhs and\
                    self.rhs == other.rhs and\
                    self.indices == other.indices

    def __repr__(self, level=0):
        ret = "\t"*level+repr(self.lhs)+"\n"
        for child in self.children:
            ret += child.__repr__(level+1)
        return ret

    def getAllChildrenAsString(self):
        ret = self.lhs + " "
        for child in self.children:
            ret += child.getAllChildrenAsString()
        return ret


class EarleyParser:
    def __init__(self, sentence, grammar, posgrammar):

        self.chartItems = defaultdict(list)

        self.sentence = sentence
        self.grammar = grammar
        self.posgrammar = posgrammar
        self.words = sentence.split(" ")
        self.columns = [defaultdict(list) for i in range(len(self.words)+1)]
        self.stack = []


    def findTreesRecursively(self, tree):
        print("tree")
        treeList = [tree]
        constituentIndex = 0
        for constituent in tree.rhs:
            if constituent != ".":
                newTrees = []
                subtreesToFindWithThisConstituent = tree.subtrees[constituentIndex]

                constituentIndex += 1
                if len(subtreesToFindWithThisConstituent) > 1:
                    subtreesFoundWithThisConstituent = []
                    for subtreeToFind in subtreesToFindWithThisConstituent:
                        subtreesFoundWithThisConstituent.extend(self.findTreesRecursively(subtreeToFind))
                    for subtreeFound in subtreesFoundWithThisConstituent:
                        for parentTree in treeList:
                            newTree = copy.deepcopy(parentTree)
                            newTree.children.append(subtreeFound)
                            newTrees.append(newTree)

                elif len(subtreesToFindWithThisConstituent) == 1:
                    for parentTree in treeList:
                        newTree = copy.deepcopy(parentTree)
                        subtreeToFind = subtreesToFindWithThisConstituent[0]
                        if subtreeToFind.subtrees != None:
                            newChildren = self.findTreesRecursively(subtreeToFind)
                            newTree.children.append(newChildren[0])

                            #THIS SHOULD NEVER HAPPEN IF IT DOES TELL ME
                            if len(newChildren) > 1:
                                print("MEOW")
                                sys.exit()
                        else:
                            newTree.children.append(subtreeToFind)
                        newTrees.append(newTree)

                treeList = newTrees
        return treeList

    def findAllParseTrees(self, treeList):
        self.trees = []
        for treeGamma in treeList:
            for tree in treeGamma:
                self.trees.extend(self.findTreesRecursively(tree))
        return self.trees


    def predictor(self, item, lhs, i):

        #PRODUCTIONS OF LHS
        toExpand = self.grammar[lhs]

        for rhSide in toExpand:
            rhs = ["."]
            rhs.extend(rhSide[0])
            newChartItem = ChartItem(lhs, rhs, [i,i], rhSide[1])
            columnLock = self.locks[i]
            columnLock.acquire()
            try:
                if newChartItem not in self.columns[i][rhs[1]]:

                    self.columns[i][rhs[1]].append(newChartItem)

                    self.stackLock.acquire()
                    try:
                        self.stack.append(newChartItem)
                    finally:
                        self.stackLock.release()
            finally:
                columnLock.release()
        self.processCount -= 1


    def scanner(self, item, indexRightOfDot, i):
        if i >= len(self.words):
            self.processCount -= 1

            return
        else:
            word = self.words[i]
            wordToLookup = word
            pos = item.rhs[indexRightOfDot]
            if word not in self.posgrammar:
                wordToLookup = "<UNK>"
            if pos in self.posgrammar[wordToLookup]:
                terminalProduction = ChartItem(pos, word, item.indices, self.posgrammar[word][pos])
                newChartItem = copy.deepcopy(item)
                newChartItem.swapDot(indexRightOfDot)
                newChartItem.incrementSecondNumber()
                newChartItem.addChild(terminalProduction, indexRightOfDot - 1)
                indexRightOfDot += 1
                key = None if indexRightOfDot == len(newChartItem.rhs) else newChartItem.rhs[indexRightOfDot]
                columnLock = self.locks[i+1]
                columnLock.acquire()
                try:
                    self.columns[i+1][key].append(newChartItem)
                finally:
                    columnLock.release()
            self.processCount -= 1


    def completer(self, item, i):

        lhs = item.lhs
        columnIndex = item.indices[0]

        for otherItem in self.columns[columnIndex][lhs]:
            rhs = otherItem.rhs
            indexRightOfDot = 1
            while rhs[indexRightOfDot-1] != ".":
                indexRightOfDot += 1
            if indexRightOfDot < len(otherItem.rhs) and self.canFitOnChart(otherItem, indexRightOfDot, i):
                newChartItem = copy.deepcopy(otherItem)
                newChartItem.swapDot(indexRightOfDot)
                newChartItem.replaceSecondNumber(item)
                newChartItem.addChild(item, indexRightOfDot - 1)
                indexRightOfDot += 1
                key = None if indexRightOfDot == len(newChartItem.rhs) else newChartItem.rhs[indexRightOfDot]
                columnLock = self.locks[i]
                columnLock.acquire()
                try:
                    if newChartItem not in self.columns[i][key]:
                        self.columns[i][key].append(newChartItem)
                        self.stackLock.acquire()
                        try:
                            self.stack.append(newChartItem)
                        finally:
                            self.stackLock.release()
                finally:
                    columnLock.release()
        self.processCount -= 1

    def parsingFuncToMapAsync(self, itemsToProcess, i):
        #self.semaphore.acquire()
        for item in itemsToProcess:
            rhs = item.rhs
            indexRightOfDot = 1
            while rhs[indexRightOfDot-1] != ".":
                indexRightOfDot += 1
            if indexRightOfDot < len(rhs):
                thingToCheck = rhs[indexRightOfDot]

                if thingToCheck in self.grammar:
                    self.processCount += 1
                    self.predictor(item, thingToCheck, i)
                else:
                    self.processCount += 1
                    self.scanner(item, indexRightOfDot, i)
            else:
                self.processCount += 1
                self.completer(item, i)
        #self.semaphore.release()


    def earleyParse(self):
        starter = ChartItem("gamma", [".", "S"], [0,0], 1.0)
        self.columns[0]["S"].append(starter)
        self.locks = [Lock() for i in range(len(self.columns))]
        self.stackLock = Lock()

        self.processCount = 0
        #self.semaphore = Semaphore()

        for i in range(len(self.columns)):
            print("column", i)
            #self.semaphore.acquire()
            self.stackLock.acquire()
            try:
                #key is the constituent right of the dot.
                for item in self.columns[i]:
                    for rule in self.columns[i][item]:
                        self.stack.append(copy.deepcopy(rule))
            finally:
                self.stackLock.release()
                #self.semaphore.release()
                while len(self.stack) > 0:
                    threads = []
                    while len(self.stack) > 0:
                        binMax = max(1,int(math.sqrt(len(self.stack))/2))
                        binMax = min(len(self.stack), 16, binMax)
                        bins =  [self.stack[i:i + binMax] for i in range(0, len(self.stack), binMax)]
                        #self.semaphore = Semaphore(value=len(bins))
                        self.stack = []
                        for bin in bins:
                            t = Thread(target=self.parsingFuncToMapAsync, args=(bin, i))
                            threads.append(t)
                            t.daemon = True
                            t.start()
                    for thread in threads:
                        if thread.is_alive():
                            thread.join()

        #self.semaphore.acquire()
        # while self.processCount > 0:
        #     pass
        #self.semaphore.release()
        for finishedParse in self.columns[-1][None]:
            if finishedParse.lhs == 'gamma':
                return finishedParse.children[0]
        return None

    def canFitOnChart(self, item, indexRightOfDot, i):
        return (len(item.rhs) - (indexRightOfDot + 2) <= (len(self.columns) - 1 - i))


