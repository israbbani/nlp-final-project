from nltk import tree
from nltk.corpus import treebank
from EarleyParser import *
from ProbCFG import *
from ParentAnnotProbCFG import *
from Tester import *
import pprint
from multiprocessing import Pool
import multiprocessing


def main():

    pCFG = ProbCFG()
    # pCFG = ParentAnnotProbCFG()
    ## this is the percentage of the corpus used for training
    trainingPercent = 0.20
    corpusFiles = treebank.fileids()
    # trainingSet = corpusFiles[:int(len(corpusFiles)*trainingPercent)]
    trainingSet = ['wsj_0001.mrg','wsj_0002.mrg', 'wsj_0003.mrg']
    testingSet = ['wsj_0001.mrg']
    ## iterate through all the files in the training set
    for cFile in trainingSet:
        print('Processing: ', cFile)
        fileText = treebank.parsed_sents(cFile)
        # iterate through each parsed sentence in the file
        for tree in fileText:
            # add the rules to the grammar
            pCFG.addRules(tree)
    ## set probabilities
    pCFG.setPCFGProbs()

    # pprint.pprint(pCFG.cfg)
    earleyGrammar = Grammar()
    earleyGrammar.addRules(pCFG.cfg)

    # earleyParser = EarleyParser("pierre vinken pierre vinken as pierre vinken", earleyGrammar.rules, earleyGrammar.posRules)

    input = ["this is a dog", "i am a cat","pierre vinken pierre vinken as pierre vinken", "dog cat dog cat as dog cat"]

    def parserFunc(sent):
        earleyParser = EarleyParser(sent, earleyGrammar.rules, earleyGrammar.posRules)
        parse = earleyParser.earleyParse()
        if parse != None:
            print(parse.stringVersion)
        else:
            print("Sorry, no parse for: ", sent)
        return

    for inp in input:

        p = multiprocessing.Process(target=parserFunc, args = (inp,))
        p.start()

    test = Tester(pCFG,[ treebank.sents(cFile) for cFile in testingSet ],[ treebank.parsed_sents(cFile) for cFile in testingSet ])
    #allTrees = earleyParser.findAllParseTrees(allTrees)
    #print(pCFG.getMostLikelyParse(allTrees)[0].stringVersion)
    # pprint.pprint(pCFG.cfg)
    # trees = [ Node('S',[ Node('NP-SBJ',[ Node('NNP', [Node ('pierre',None)]), Node('NNP', [Node ('vinken',None)]) ]),
    # 		  Node('NP-PRD', [
    # 							Node ('NP', [ Node('NNP', [Node ('pierre',None)]), Node('NNP', [Node ('vinken',None)]) ]),
    # 							Node ('PP', [
    # 											Node('IN',[Node('as',None)]),
    # 											Node ('NP', [ Node('NNP', [Node ('pierre',None)]), Node('NNP', [Node ('vinken',None)]) ])
    # 										])
    # 						  ])
    # 					]),
    # 		 Node('S',[ Node('NP-SBJ',[ Node('NNP', [Node ('pierre',None)]), Node('NNP', [Node ('vinken',None)]) ]),
    # 		  Node('NP-PRD', [
    # 							Node ('NP', [ Node('NNP', [Node ('pierre',None)]), Node('NNP', [Node ('vinken',None)]) ]),
    # 							Node ('PP', [
    # 											Node('IN',[Node('of',None)]),
    # 											Node ('NP', [ Node('NNP', [Node ('pierre',None)]), Node('NNP', [Node ('vinken',None)]) ])
    # 										])

    # 						  ])
    # 					])
    # 		]
    # print(pCFG.getMostLikelyParse(trees))

if __name__ == '__main__':
    main()