##
# Earley Algorithm Parser
# returns a 'chart'
##
class ChartItem:
    def __init__(self, lhs, rhs, indices):
        self.lhs = lhs
        self.rhs = rhs
        self.indices = indices
        self.subtrees = [[] for i in range(len(self.rhs)-1)] if type(rhs) is list else None
        self.children = []
        self.stringVersion = self.lhs + "| [" + ", ".join(self.rhs)  + " ] | (" + ", ".join(map(str, self.indices)) + ")"

    def addChild(self,child, rhsIndex):
        self.subtrees[rhsIndex].append(child)

    def swapDot(self, indexRightOfDot):
        self.rhs[indexRightOfDot], self.rhs[indexRightOfDot-1] = self.rhs[indexRightOfDot -1], self.rhs[indexRightOfDot]
    def incrementSecondNumber(self):
        self.indices[1] += 1
    def replaceSecondNumber(self, otherChartItem):
        self.indices[1] = otherChartItem.indices[1]


    def __eq__(self, other):
        return self.lhs == other.lhs and\
                self.rhs == other.rhs and\
                self.indices == other.indices
