import string, math

class ParentAnnotProbCFG:

	def __init__(self):
		## training variables
		self.name = 'Parent Annotated'
		self.punctuation = set(string.punctuation)
		self.cfg = {}
		self.totalRules = 0
		## testing variables
		self.precision = 0.0
		self.recall = 0.0

	##
	# Takes a parsed sentence of the NLTK Treebank Corpus and adds
	# the rules and their counts to a dictionary
	##
	def addRules(self,tree):
		leaves = tree.leaves()
		parentLabel = ''
		# if not self.onlyPunctuation(tree.label()):
		# 	parentLabel = tree.label()
		## iterate through each line
		for subtree in tree.subtrees():
			print('~~~~~~~~')
			print(subtree)
			print('~~~~~~~~')
			if not self.onlyPunctuation(subtree.label()):
				## set the LHS symbol
				lhs = subtree.label()
				if parentLabel != '':
					lhs = lhs + '^' + parentLabel
				print(lhs)
				parentLabel = subtree.label()
				if (not self.onlyPunctuation(lhs)):
					rhs = []
					for token in subtree:
						## if it is a terminal
						if token in leaves:
							if not self.onlyPunctuation(token):
								rhs.append(token.lower())
						## if non terminal
						elif not self.onlyPunctuation(token.label()):
							rhs.append(token.label() + '^' + parentLabel)
					if self.isMixedRule(rhs):
						self.addMixedRule(lh,rhs)
					else:
						self.addRuleToCFG(lhs,rhs)

	##
	# takes a list of parse trees and calculates the probability of each tree
	# returns the most likely tree and its log probability
	##
	def getMostLikelyParse(self,trees):
		maxProb = -float('inf')
		mostLikelyParse = None
		for tree in trees:
			treeProb = self.getTreeProbRec(tree,None)
			if treeProb > maxProb:
				maxProb = treeProb
				mostLikelyParse = tree
		return (mostLikelyParse,maxProb)

	##
	# helper method that recursively calculates probability of the parse tree
	##
	def getTreeProbRec(self,node,parent):
		## if node is terminal
		if (node.children == None or len(node.children) == 0):
			return 0
		## if not non-terminal
		else:
			rhs = [child.lhs for child in node.children]
			childProb = 0
			for child in node.children:
				childProb += self.getTreeProbRec(child,node)
			return math.log(self.getRuleProb(node.lhs,rhs)) + childProb

	##
	# Helper method, takes in a string and returns True if
	# it contains only punctuation
	##
	def onlyPunctuation(self,inputStr):
		return not any(char.isalnum() for char in inputStr)
	##
	# rhs is a list of symbols
	# objList is a list of rhs objects
	# returns the index of rhs in objList if rhs is in objList
	# returns None if rhs not in objList
	##
	def rhsInList(self,rhs,objList):
		for i in range(len(objList)):
			if (objList[i]['rhs'] == rhs):
				return i
		return None

	##
	# takes in a pCFG (dictionary) and uses counts to create
	# probabilities
	# returns pCFG with probabilities
	##
	def setPCFGProbs(self):
		for lhs in self.cfg:
			for i in range(len(self.cfg[lhs]['rules'])):
				self.cfg[lhs]['rules'][i]['prob'] = self.cfg[lhs]['rules'][i]['count']/self.totalRules

	##
	# takes in a lhs (string), and rhs (list) and looks through the grammar
	# to find the prob of lhs -> rhs and returns it. Returns None if rule not found
	##
	def getRuleProb(self,lhs,rhs):
		print(lhs,rhs)
		for rule in self.cfg[lhs]['rules']:
			if (rule['rhs'] == rhs):
				return rule['prob']
		return None

	## 
	# takes in the rhs of a rule to see if it contains a mix of POS and Non-POS
	# returns true if it contains POS and Non-POS
	##
	def isMixedRule(self,rhs):
		pos = False
		nonPos = False
		for symbol in rhs:
			if (symbol.islower()):
				nonPos = True
			elif (symbol.isupper()):
				pos = True
		if pos and nonPos:
			return True
		else:
			return False

	##
	# takes in a rule with that has both pos and nonpos on the rhs and converts
	# to a rule with just pos and a dummy rule
	# A -> B and C
	# turns into A -> B AND C + AND -> and
	# adds the rules to the grammar
	##
	def addMixedRule(self,lhs,rhs):
		modifiedRhs = []
		for symbol in rhs:
			# non pos: add dummy rule
			if symbol.islower():
				# add dummy rule to grammar
				self.addRuleToCFG(symbol.upper(),symbol)
				modifiedRhs.append(symbol.upper())
			else:
				modifiedRhs.append(symbol)
		addRuleToCFG(lhs,modifiedRhs)

	##
	# adds rule to pCFG and increments total rules count
	##
	def addRuleToCFG(self,lhs,rhs):
		## if we have already seen the LHS symbol before
		if lhs in self.cfg:
			## if we have also seen the RHS symbols before i.e. seen the rule before
			# if rhs in self.cfg[lhs]['rules']:
			listIndex = self.rhsInList(rhs,self.cfg[lhs]['rules'])
			if listIndex != None:
				self.cfg[lhs]['rules'][listIndex]['count'] += 1
			## if we have seen the LHS symbol but not this rule
			else:
				self.cfg[lhs]['rules'].append({
					"rhs":rhs,
					"count":1
					})
			self.cfg[lhs]['count'] += 1
		## if we haven't seen the LHS symbol before
		else:
			self.cfg[lhs] = {
				"rules":[{
					"rhs":rhs,
					"count":1
				}],
				"count":1
			}
		self.totalRules += 1

	##
	# takes a list of parse trees and calculates the probability of each tree
	# returns the most likely tree and its log probability
	##
	def getMostLikelyParse(self,trees):
		maxProb = -float('inf')
		mostLikelyParse = None
		for tree in trees:
			treeProb = self.getTreeProbRec(tree,None)
			if treeProb > maxProb:
				maxProb = treeProb
				mostLikelyParse = tree
		return (mostLikelyParse,maxProb)

	##
	# helper method that recursively calculates probability of the parse tree
	##
	def getTreeProbRec(self,node,parent):
		## if node is terminal
		if (node.children == None or len(node.children) == 0):
			return 0
		## if not non-terminal
		else:
			rhs = [child.lhs for child in node.children]
			childProb = 0
			for child in node.children:
				childProb += self.getTreeProbRec(child,node)
			return math.log(self.getRuleProb(node.lhs,rhs)) + childProb